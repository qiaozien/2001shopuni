import httprequest from '../utils/httprequest';

//首页轮播图接口
export const indexImgs = params => httprequest.get('/indexImgs', params);
//首页列表的接口
export const prodListByTagId = params => httprequest.get('/prod/prodListByTagId', params);

//登录
export const login = params => httprequest.post('/login?grant_type=mini_app', params);
// 地址选择

export const listByPid = params => httprequest.get('/p/area/listByPid', params);
