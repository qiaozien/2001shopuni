// #ifndef VUE3
import Vue from 'vue';
import App from './App';
import store from './store/index';
import plugins from './plugins/index';
Vue.config.productionTip = false;

App.mpType = 'app';
Vue.use(plugins, { title: '2001A' });
const app = new Vue({
    store,
    ...App,
});
app.$mount();
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue';
import App from './App.vue';
export function createApp() {
    const app = createSSRApp(App);
    return {
        app,
    };
}
// #endif
