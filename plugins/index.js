// export default function install() {}
import { mapState } from 'vuex';
export default {
    install(Vue, options) {
        console.log(options, 'options12');
        Vue.prototype.$http = 123;
        //全局混入
        Vue.mixin({
            data() {
                return {};
            },
            computed: {
                ...mapState('car', ['total']),
            },
            onShow() {
                console.log('onshow');
                this.setTabBarBadge();
            },
            methods: {
                setTabBarBadge() {
                    uni.setTabBarBadge({
                        index: 2,
                        text: this.total + '',
                    });
                },
            },
        });
        // Vue.component()
    },
};
