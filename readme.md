1. css 单行文本的溢出？多行文本的溢出？
2. vuex 常用的模块？

-   state 是存放共享状态
-   getters 是派生新状态
-   mutation 同步的方法（修改状态的唯一途径，就是提交 mutations 里面的方法）
-   action 异步的方法
-   modules 模块

3. 在组件里面的使用

-   state this.$store.state.total

-   辅助函数 参数：数组/对象 返回值:对象
-   属性：computed (state,getter)
-   方法：method (mutation action)

4. watch 和 computed

-   computed 计算属性。有一个缓存的作用。当它所依赖的值发生变化的时候，不会更新
-   watch 侦听数据的。 不会立即执行，当侦听的数据发生变化的时候才会执行。
-   侦听函数接受 2 个参数，第一个是新值，第二个是老值
-   设置 immediate：true。可以立即执行侦听函数
-   设置 deep: true。可以开启深度监听

5. mixins（抽离复用的功能逻辑） 混入规则

-   如果混入里面有 data,它会和组件里面的 data 合并，当 key 值相同的时候，会保留组件的状态
-   如果混入里面有生命周期的话，会合并到数组里面，都会执行
-   如果混入里面有 methods\computed，也会合并都会生效，当 key 相同的时候，会保留组件的

6. 局部混入和全局混入

7. 自定义插件

-   新建一个 plugins 文件夹，创建一个 idnex.js
-   在文件内部创建一个 install 方法。（如果是对象，对象里面一个要有一个 install 的方法。如果是函数，那么这个函数就会被当做 install 方法）
-   在 main.js 里面，Vue.use(插件名,插件的参数)

8. eventbus 发布订阅

9. 判断是否是数组有几种方式？
