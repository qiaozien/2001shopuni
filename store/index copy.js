import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex); //注册插件 会去调用这个插件内部的install方法。同时把vue当参数传给install这个函数
const store = new Vuex.Store({
    state: {
        total: 0,
        user: [],
    },
    mutations: {
        changeTotal(state) {
            state.total++;
        },
        changeUser() {},
    },
});

export default store;
