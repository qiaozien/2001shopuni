import { login } from '@/api/api';
const user = {
    namespaced: true, //开启命名空间
    state: () => ({
        //数据共享
        token: uni.getStorageSync('token') || '',
        userInfo: uni.getStorageSync('userInfo') || {},
    }),
    mutations: {
        changeInfo(state, { token, userInfo }) {
            // console.log(payload, 'payload');
            state.token = token;
            state.userInfo = userInfo;
            this.commit('user/saveStoreage');
        },
        saveStoreage(state) {
            uni.setStorageSync('token', state.token);
            uni.setStorageSync('userInfo', state.userInfo);
        },
        removeStoreage(state) {
            state.token = '';
            state.userInfo = {};
            uni.removeStorageSync('token', state.token);
            uni.removeStorageSync('userInfo', state.userInfo);
        },
    },
    actions: {
        async getToken({ state, commit }, { code, userInfo }) {
            let res = await login({
                principal: code,
            });
            commit('changeInfo', { token: res.access_token, userInfo });
            console.log(res, 'res*****');
        },
        async isLogin({ state }) {
            console.log(state.token, 'tokne');
            if (state.token) return true; //登录过
            const [error, res] = await uni.showModal({
                title: '登录',
                content: '当前没有登录，请先登录',
            });
            if (res.confirm) {
                uni.navigateTo({
                    url: '/searchpkg/login-page/login-page',
                });
            }
            return false;
        },
    },
};

export default user;
