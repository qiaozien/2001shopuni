const total = {
    namespaced: true, //开启命名空间
    state: () => ({ total: 1 }),
    mutations: {
        changeTotal(state) {
            state.total++;
        },
    },
};

export default total;
