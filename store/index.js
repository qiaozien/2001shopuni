import Vue from 'vue';
import Vuex from 'vuex';
import total from './modules/total';
import user from './modules/user';
Vue.use(Vuex); //注册插件 会去调用这个插件内部的install方法。同时把vue当参数传给install这个函数
const store = new Vuex.Store({
    modules: {
        car: total,
        user,
    },
});

export default store;
