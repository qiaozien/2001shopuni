import { mapState } from 'vuex';
export default {
    data() {
        return {};
    },
    computed: {
        ...mapState('car', ['total']),
    },
    onShow() {
        console.log('onshow');
        this.setTabBarBadge();
    },
    methods: {
        setTabBarBadge() {
            uni.setTabBarBadge({
                index: 2,
                text: this.total + '',
            });
        },
    },
};
