import { base_url } from './config.js';

class HttpRequest {
    request(url, method, params) {
        //相当于es5 HttpRequest.prototype.request = function(){}
        uni.showLoading({
            title: '正在加载中...',
        });
        return new Promise((resolve, reject) => {
            uni.request({
                url: base_url + url, //仅为示例，并非真实接口地址。
                data: params,
                method,
                header: {
                    Authorization: `bearer${
                        uni.getStorageSync('token') || '53a12f9c-2988-4ff1-a619-72b95161d3ea'
                    }`,
                },
                success: res => {
                    if (res.statusCode == 200) {
                        resolve(res.data);
                        uni.hideLoading();
                    }
                },
                fail: err => {
                    reject(err);
                    uni.hideLoading();
                    uni.showToast({
                        title: '请求失败',
                        duration: 2000,
                    });
                },
                complete: () => {
                    //不管请求成功或者失败都会执行的函数
                    uni.hideLoading();
                },
            });
        });
    }
    get(url, params) {
        console.log(url, params, 'params');
        return this.request(url, 'GET', params);
    }
    post(url, params) {
        // var obj = {url,methos:'post',params}
        return this.request(url, 'POST', params);
    }
}

export default new HttpRequest();
