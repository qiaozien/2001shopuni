// Vue.prototype.$bus = new Vue();
// this.$bus.emit('click','123')
// this.$bus.on('click',(aa)=>{})

class EventBus {
    constructor() {
        this.event = {};
    }
    on(eventname, ck) {
        if (this.event[eventname] instanceof Array) {
            //不是第一次
            this.event[eventname].push(ck);
        } else {
            this.event[eventname] = [ck];
        }
    }
    emit(eventname, ...agrs) {
        this.event[eventname] && this.event[eventname].map(fn => fn(agrs));
    }
}

let bus = new EventBus();

//event =  {
//     add:[fn1,fn2,fn3],
//     del:[fn1],

// }
bus.on('add', val => {
    console.log(val, 'add');
});
bus.on('add', val1 => {
    console.log(val1, 'add123');
});

bus.on('del', val => {
    console.log(val, 'del');
});

bus.emit('add', 123);
bus.emit('del', 456);
